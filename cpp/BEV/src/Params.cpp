#include <cmath>

#include "inc/Params.h"

using namespace bev;

Params::Params(const double resolution,
               const std::pair<const double, const double> xLims,
               const std::pair<const double, const double> zLims)
    : transformRes(resolution),
      xLimits(xLims), zLimits(zLims),
      transformSize(std::make_pair<const size_t, const size_t>(
              std::round((xLimits.second - xLimits.first) / transformRes),
              std::round((zLimits.second - zLimits.first) / transformRes)))
{}

#include "inc/ReverseTransform.h"

#include <cstdio>
#include <cmath>
#include <cassert>

#include <utility>

#ifdef _MSC_VER 
#if (_MSC_VER < 1800) // Visual studio 2013
#error "You are not able to compile without C++ 11 support. Upgrade your visual studio."
#else
#include <numeric> // iota is here when using msvc
#endif // (_MSC_VER < 1800)
#endif // _MSC_VER 

#include <algorithm>

#include "inc/Utils.h"
#include "inc/MathHelpers.h"

using namespace bev;

ReverseTransform::ReverseTransform(const ParsedConfig& cnfg,
                 std::pair<const size_t, const size_t> outSize)
    : Transform(cnfg), outImgSize(outSize)
{}

matrix<double> ReverseTransform::image2world(const matrix<double>& uvMat) const NOEXCEPT
{
    auto tr33Inverted = bev::math::invert<double>(tr33);
    matrix<double> result = prod(tr33Inverted, uvMat);

    for (matSizeType j = 0 ; j < result.size2() ; ++j)
    {
        for (matSizeType i = 0 ; i < result.size1() ; ++i)
        {
            result(i, j) = result(i, j) / result(result.size1() - 1, j);
        }
    }

    return std::move(result);
}

std::pair<vector<ulong>, vector<ulong> > ReverseTransform::computeXimAndYim(const size_t hei,
                                                                            const size_t wid)
																			const NOEXCEPT
{
    auto totalSize = hei * wid;
    vector<ulong> xim(totalSize);
    vector<ulong> yim(totalSize);

    std::vector<ulong> data(wid);
    std::iota(data.begin(), data.end(), 1);

    auto widCnt = 0;
    for (matSizeType i = 0 ; i < hei ; ++i)
    {
        std::copy(data.begin(), data.end(), xim.begin() + widCnt);

        auto begin = yim.begin() + widCnt;
        auto end = begin + wid;
        std::fill(begin, end, i % wid + 1);

        widCnt += wid;
    }

    return std::make_pair(xim, yim);
}

void ReverseTransform::computeLookUpTable() NOEXCEPT
{
    const size_t resultHei = outImgSize.second;
    const size_t resultWid = outImgSize.first;
    const size_t totalPixelsCnt = resultWid * resultHei;

    auto ims = computeXimAndYim(resultHei, resultWid);
    const vector<ulong>& xim = ims.first;
    const vector<ulong>& yim = ims.second;
    vector<ulong> ones(totalPixelsCnt);

    std::fill(ones.begin(), ones.end(), 1);

    matrix<double> uvMat(3, totalPixelsCnt);
    row(uvMat, 0) = xim;
    row(uvMat, 1) = yim;
    row(uvMat, 2) = ones;

    auto xzMat = image2world(uvMat);

    const vector<double>& x = row(xzMat, 0);
    const vector<double>& z = row(xzMat, 1);

    vector<ulong> xBevIndReverseAll(totalPixelsCnt);
    vector<ulong> zBevIndReverseAll(totalPixelsCnt);

    for (size_t i = 0 ; i < totalPixelsCnt ; ++i)
    {
        const double xiMovedAndScaled = (x[i] - params.xLimits.first) / params.transformRes;
        const double ziMovedAndScaled = (z[i] - params.zLimits.first) / params.transformRes;

        xBevIndReverseAll[i] = static_cast<ulong>(round(xiMovedAndScaled));
        zBevIndReverseAll[i] = static_cast<ulong>(round(outImgSize.first - ziMovedAndScaled));
    }

    auto selectedBevIndicies = utils::select(xBevIndReverseAll, zBevIndReverseAll,
                                             xBevIndReverseAll, zBevIndReverseAll,
                                             outImgSize);

    bevXIndValid = selectedBevIndicies.first;
    bevZIndValid = selectedBevIndicies.second;

    auto selectedImIndicies = utils::select(xBevIndReverseAll,
                                            zBevIndReverseAll,
                                            xim, yim,
                                            outImgSize);

    xImValid = selectedImIndicies.first;
    zImValid = selectedImIndicies.second;
}

image ReverseTransform::compute(const image& input)
{
    image result(cv::Size(outImgSize.first,
                          outImgSize.second),
                          input.type(), input.channels());

    computeLookUpTable();

    assert(xImValid.size() == zImValid.size()
           && bevXIndValid.size() == bevZIndValid.size()
           && bevXIndValid.size() == zImValid.size());

    for (int channelIdx = 0 ; channelIdx < input.channels() ; ++channelIdx)
    {
        for (matSizeType i = 0 ; i < bevXIndValid.size() ; ++i)
        {
            auto bevZVal = bevZIndValid(i) - 1;
            auto bevXVal = bevXIndValid(i) - 1;
            auto xImVal = xImValid(i) - 1;
            auto yImVal = zImValid(i) - 1;

            result.at<cv::Vec3b>(yImVal, xImVal)[channelIdx]
                    = input.at<cv::Vec3b>(bevZVal, bevXVal)[channelIdx];
        }
    }

    return result;
}

#include "inc/Transform.h"

#include <algorithm>

#include "inc/MathHelpers.h"

using namespace bev;

// TODO Replace hardcoded params
Transform::Transform(const ParsedConfig& cnfg)
    : tr33(3, 3), params(0.05f,
                         std::make_pair<const double, const double>(-10.0f, 10.0f),
                         std::make_pair<const double, const double>(6.0f, 46.0f))
{
    matrix<double> expandedR0(cnfg.R0);
    expandedR0.resize(4, 4);
    std::vector<double> addition = { 0, 0, 0, 1 };

    matrix_row<matrix<double> > lastRowER0(expandedR0, 3);
    std::copy(addition.begin(), addition.end(), lastRowER0.begin());

    matrix_column<matrix<double> > lastColumnER0(expandedR0, 3);
    std::copy(addition.begin(), addition.end(), lastColumnER0.begin());

    matrix<double> tmpR0 = prod(cnfg.P2, expandedR0);

    auto trCamToRoad(cnfg.TrCamToRoad);
    trCamToRoad.resize(4, 4);

    matrix_row<matrix<double> > lastRowTrCR(trCamToRoad, 3);
    std::copy(addition.begin(), addition.end(), lastRowTrCR.begin());

    auto inversedTrCamToRoad = bev::math::invert<double>(trCamToRoad);

    matrix<double> tr = prod(tmpR0, inversedTrCamToRoad);

    column(tr33, 0) = column(tr, 0);
    column(tr33, 1) = column(tr, 2);
    column(tr33, 2) = column(tr, 3);
}

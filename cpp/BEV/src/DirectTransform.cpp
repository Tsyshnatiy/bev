#include "inc/DirectTransform.h"

#include <cstdio>
#include <cmath>
#include <cassert>

#include <utility>

#ifdef _MSC_VER 
#if (_MSC_VER < 1800) // Visual studio 2013
#error "You are not able to compile without C++ 11 support. Upgrade your visual studio."
#else
#include <numeric> // iota is here when using msvc
#endif // (_MSC_VER < 1800)
#endif // _MSC_VER 

#include <algorithm>

#include "inc/Utils.h"
#include "inc/MathHelpers.h"

using namespace bev;

matrix<double> DirectTransform::computeUvMat(const size_t hei, const size_t wid) const NOEXCEPT
{
    const size_t totalPixelsCnt = hei * wid;
    vector<double> xGrid(wid);
    vector<double> zGrid(hei);

    double xGridNextVal = params.xLimits.first + params.transformRes / 2;
    for (matSizeType i = 0 ; i < xGrid.size() ; ++i)
    {
        xGrid(i) = xGridNextVal;
        xGridNextVal += params.transformRes;
    }

    double zGridNextVal = params.zLimits.second - params.transformRes / 2;
    for (matSizeType i = 0 ; i < zGrid.size() ; ++i)
    {
        zGrid(i) = zGridNextVal;
        zGridNextVal -= params.transformRes;
    }

    vector<double> xMeshGrid(totalPixelsCnt);
    vector<double> zMeshGrid(totalPixelsCnt);
    vector<double> ones(totalPixelsCnt);

    auto heiCnt = 0;
    for (matSizeType i = 0 ; i < wid ; ++i)
    {
        auto begin = xMeshGrid.begin() + heiCnt;
        auto end = begin + hei;

        std::fill(begin, end, xGrid(i % hei));
        std::copy(zGrid.begin(), zGrid.end(), zMeshGrid.begin() + heiCnt);

        heiCnt += hei;
    }

    std::fill(ones.begin(), ones.end(), 1);

    matrix<double> uvMat(3, totalPixelsCnt);
    row(uvMat, 0) = xMeshGrid;
    row(uvMat, 1) = zMeshGrid;
    row(uvMat, 2) = ones;

    return uvMat;
}

matrix<double> DirectTransform::world2image(const matrix<double>& uvMat) const NOEXCEPT
{
    matrix<double> result = prod(tr33, uvMat);

    for (matSizeType j = 0 ; j < result.size2() ; ++j)
    {
        for (matSizeType i = 0 ; i < result.size1() ; ++i)
        {
            result(i, j) = result(i, j) / result(result.size1() - 1, j);
        }
    }

    return result;
}

void DirectTransform::computeLookUpTable() NOEXCEPT
{
    const size_t resultHei = params.transformSize.second;
    const size_t resultWid = params.transformSize.first;

    const size_t totalPixelsCnt = resultWid * resultHei;

    auto uvMat = computeUvMat(resultHei, resultWid);

    auto transformedToImage = world2image(uvMat);

    const vector<double>& xi = row(transformedToImage, 0);
    const vector<double>& zi = row(transformedToImage, 1);

    auto validPixelsVectors = utils::select(xi, zi, xi, zi,
                                            inputImageSize);

    uMat = validPixelsVectors.first;
    vMat = validPixelsVectors.second;

    vector<ulong> validXPixIndicies(totalPixelsCnt);
    vector<ulong> validZPixIndicies(totalPixelsCnt);

    auto heiCnt = 0;
    std::vector<ulong> data(resultHei);
    std::iota(data.begin(), data.end(), 1);

    for (matSizeType i = 0 ; i < resultWid ; ++i)
    {
        auto begin = validXPixIndicies.begin() + heiCnt;
        auto end = begin + resultHei;

        std::fill(begin, end, i % resultHei + 1);
        std::copy(data.begin(), data.end(), validZPixIndicies.begin() + heiCnt);

        heiCnt += resultHei;
    }

    auto indicies = utils::select(xi, zi,
                                  validXPixIndicies,
                                  validZPixIndicies,
                                  inputImageSize);

    bevXIndicies = indicies.first;
    bevZIndicies = indicies.second;
}

image DirectTransform::compute(const image& input)
{
    image result(cv::Size(params.transformSize.first,
                          params.transformSize.second),
                          input.type(), input.channels());

	inputImageSize = std::make_pair<size_t, size_t>((size_t)input.rows, (size_t)input.cols);

    computeLookUpTable();

    assert(vMat.size() == uMat.size()
           && bevXIndicies.size() == bevZIndicies.size()
           && vMat.size() == bevXIndicies.size());

    for (int channelIdx = 0 ; channelIdx < input.channels() ; ++channelIdx)
    {
        for (matSizeType i = 0 ; i < bevZIndicies.size() ; ++i)
        {
            auto bevZVal = bevZIndicies(i) - 1;
            auto bevXVal = bevXIndicies(i) - 1;
            auto vMatVal = static_cast<ulong>(vMat(i)) - 1;
            auto uMatVal = static_cast<ulong>(uMat(i)) - 1;

            result.at<cv::Vec3b>(bevZVal, bevXVal)[channelIdx]
                    = input.at<cv::Vec3b>(vMatVal, uMatVal)[channelIdx];
        }
    }

    return result;
}


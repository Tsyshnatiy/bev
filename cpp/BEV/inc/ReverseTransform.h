#pragma once

#include "inc/Transform.h"

namespace bev
{
    class ReverseTransform final : public Transform
    {
    private:
        std::pair<const size_t, const size_t> outImgSize;
        vector<ulong> bevXIndValid;
        vector<ulong> bevZIndValid;
        vector<ulong> xImValid;
        vector<ulong> zImValid;

        std::pair<vector<ulong>, vector<ulong> > computeXimAndYim(const size_t hei,
                                                                  const size_t wid)
                                                                  const NOEXCEPT;

        matrix<double> image2world(const matrix<double>& uvMat) const NOEXCEPT;

    protected:
        void computeLookUpTable() NOEXCEPT override;

    public:
        ReverseTransform(const ParsedConfig& cnfg,
                         std::pair<const size_t, const size_t> outImgSize);

        image compute(const image& input) override;
    };
} // bev

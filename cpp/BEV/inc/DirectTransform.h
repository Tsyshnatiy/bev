#pragma once

#include "inc/Transform.h"

namespace bev
{
    class DirectTransform final : public Transform
    {
    private:
        vector<ulong> bevXIndicies;
        vector<ulong> bevZIndicies;
        vector<double> uMat;
        vector<double> vMat;
        std::pair<size_t, size_t> inputImageSize;

		matrix<double> computeUvMat(const size_t hei, const size_t wid) const NOEXCEPT;

		matrix<double> world2image(const matrix<double>& uvMat) const NOEXCEPT;

    protected:
        void computeLookUpTable() NOEXCEPT override;

    public:
        DirectTransform(const ParsedConfig& cnfg)
            : Transform(cnfg) {}

        image compute(const image& input) override;
    };
} // bev

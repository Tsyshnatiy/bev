#pragma once

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "inc/Exceptions.h"

namespace bev
{
namespace math
{
    using namespace boost::numeric::ublas;

    template<class T>
    matrix<T> invert(matrix<T> input)
    {
        using pmatrix = permutation_matrix<std::size_t>;

        matrix<T> result(input.size1(), input.size2());
        pmatrix pm(input.size1());
        if (lu_factorize(input, pm))
        {
            throw math_exception("Lu factorization failed");
        }

        result.assign(identity_matrix<T>(input.size1()));
        lu_substitute(input, pm, result);

        return result;
    }
}
}

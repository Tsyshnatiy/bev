#pragma once

#include <cstdio>

#ifdef __unix__
#include <opencv2/core/core.hpp>
#else
#include <opencv2/core.hpp>
#endif

#include <boost/numeric/ublas/io.hpp>

#include "inc/Params.h"
#include "inc/Utils.h"
#include "inc/ConfigParser.h"

namespace bev
{
    using namespace boost::numeric::ublas;

    using ulong = unsigned long;
    using image = cv::Mat;
    using matSizeType = matrix<double>::size_type;

    class Transform
    {
    protected:
        matrix<double> tr33;
        Params params;

        virtual void computeLookUpTable() NOEXCEPT = 0;

    public:
        Transform(const ParsedConfig& cnfg);

        virtual image compute(const image& input) = 0;
    };
} // bev

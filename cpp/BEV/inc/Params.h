#pragma once

#include <cstdio>
#include <utility>

namespace bev
{
    struct Params
    {
    public:
        const double transformRes;
        const std::pair<const double, const double> xLimits;
        const std::pair<const double, const double> zLimits;
        const std::pair<const size_t, const size_t> transformSize;

        Params(const double resolution,
               const std::pair<const double, const double> xLimits,
               const std::pair<const double, const double> zLimits);
    };
}

#pragma once

#include <stdexcept>

namespace bev
{
    class bev_exception : public std::runtime_error
    {
    public:
        bev_exception(const std::string& message) : std::runtime_error(message) {}
        virtual ~bev_exception(){}
    };

    class parser_exception : public bev_exception
    {
    public:
        parser_exception(const std::string& message) : bev_exception(message) {}
        virtual ~parser_exception(){}
    };

    class no_such_file_exception : public parser_exception
    {
    public:
        no_such_file_exception(const std::string& message) : parser_exception(message) {}
        virtual ~no_such_file_exception(){}
    };

    class invalid_config_format_exception : public parser_exception
    {
    public:
        invalid_config_format_exception(const std::string& message) : parser_exception(message) {}
        virtual ~invalid_config_format_exception(){}
    };

    class json_node_does_not_exist : public parser_exception
    {
    public:
        json_node_does_not_exist(const std::string& message) : parser_exception(message) {}
        virtual ~json_node_does_not_exist(){}
    };

    class math_exception : public parser_exception
    {
    public:
        math_exception(const std::string& message) : parser_exception(message) {}
        virtual ~math_exception(){}
    };
}

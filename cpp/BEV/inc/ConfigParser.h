#pragma once

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace bev
{
    using namespace boost::numeric::ublas;

    struct ParsedConfig
    {
        // Upper case because of specification
        matrix<double> P0;
        matrix<double> P1;
        matrix<double> P2;
        matrix<double> R0;
        matrix<double> TrVeloToCam;
        matrix<double> TrImuToVelo;
        matrix<double> TrCamToRoad;

        ParsedConfig();
    };

    class ConfigParser final
    {
    private:
        const std::string p0_key = "P0.elems";
        const std::string p1_key = "P1.elems";
        const std::string p2_key = "P2.elems";
        const std::string r0_key = "R0.elems";
        const std::string trV2CamKey = "Tr_velo_to_cam.elems";
        const std::string trI2VKey = "Tr_imu_to_velo.elems";
        const std::string trC2RKey = "Tr_cam_to_road.elems";

        matrix<double> getMatrixByJsonKey(const boost::property_tree::ptree& pt,
                                          const std::string& key,
                                          const matrix<double>::size_type rows,
                                          const matrix<double>::size_type cols) const;
    public:
        ParsedConfig parse(const std::string& config_name) const;
    };
}

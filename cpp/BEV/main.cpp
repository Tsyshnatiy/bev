#include <iostream>
#include <memory>

#include <opencv2/opencv.hpp>

#include "inc/ConfigParser.h"
#include "inc/Params.h"

#include "inc/Exceptions.h"

#include "inc/DirectTransform.h"
#include "inc/ReverseTransform.h"

// av[1] - path to config
// av[2] - path to input image
// av[3] - path to output image
// av[4] - rev | dir (reverse or direct)
int main(int ac, char* av[])
{
    if (ac != 5)
    {
        std::cout << "Invalid input arguments number" << std::endl;
        return 1;
    }

    bev::ConfigParser parser;
    auto config = parser.parse(av[1]);

    auto image = cv::imread(av[2], CV_LOAD_IMAGE_UNCHANGED);

    std::shared_ptr<bev::Transform> tr = nullptr;
    if (std::string(av[4]) == "rev")
    {
        tr = std::shared_ptr<bev::Transform>(new bev::ReverseTransform(config, std::make_pair(800, 400)));
    }
    else if (std::string(av[4]) == "dir")
    {
        tr = std::shared_ptr<bev::Transform>(new bev::DirectTransform(config));
    }
    else
    {
        std::cout << "Invalid transform type requested" << std::endl;
        return 1;
    }

    try
    {
        auto result = tr->compute(image);

        std::cout << "Saving to : " << std::string(av[3]) << std::endl;
        cv::imwrite(av[3], result);
    }
    catch (bev::bev_exception& ex)
    {
        std::cout << "Unable to perform compute. Details: " << std::string(ex.what()) << std::endl;
    }
    catch (std::runtime_error& ex)
    {
        std::cout << "Unable to save image. Details: " << std::string(ex.what()) << std::endl;
    }

    return 0;
}

